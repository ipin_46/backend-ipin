var {mongoose} = require('../config/db')

var shoes = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  // address: {
  //   type: String,
  //   trim: true,
  //   required: true
  // },
  image: {
    type: String,
    trim: true,
    required: true
  },
  brand: {
    type: String,
    trim: true,
    required: true
  },
  price: {
    type: String,
    trim: true,
    required: true
  },
  desc: {
    type: String,
    trim: true,
    required: true
  },
  featured: {
    type: Boolean,
    trim: true,
    required: true
  }
})



var Shoes = mongoose.model('shoes', shoes)

module.exports = {
  Shoes: Shoes
}