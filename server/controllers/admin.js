const express = require('express')
const router = express.Router()
const multer = require('multer')
const fs = require('fs')

// Model
const {Shoes} = require('../models/shoes')

var imagepath = 'tmp/my-uploads/'

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, imagepath)
  },
  filename: function (req, file, cb) {
    var extension = file.originalname.substring(file.originalname.lastIndexOf('.'))
    cb(null, file.fieldname + '-' + Date.now() + extension) 
  }
})
 
var upload = multer({ storage: storage })

// Insert
router.post('/insert', upload.single('image'), function (req, res) {
  try {
    // res.send(req.file)
    var img = req.file.filename

    var myobj = { 
      name: req.body.name, 
      address: req.body.address,
      image: img,
      brand: req.body.brand,
      price: req.body.price,
      desc: req.body.desc,
      featured: req.body.featured
      // colbg: req.body.colbg

    }
    const shoes = new Shoes(myobj)
    shoes.save()
    res.status(201).send("1 document inserted")
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Find One
router.get('/findOne/:id', async function (req, res) {
  try {
    const id = req.params.id
    var filter = {
      _id: id
    }
    const shoes = await Shoes.findOne(filter)
    shoes.image = 'http://localhost:3000/img/'+shoes.image
    res.send(shoes)
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Find All + featured
router.get('/findAll', async function (req, res) {
  try {
    const shoes = await Shoes.find({})
    const shoesData = []
    if (shoes) {
      for (var index in shoes) {
        shoesData.push({
          id: shoes[index]._id,
          title: shoes[index].name,
          desc: shoes[index].desc,
          image: 'http://localhost:3000/img/' + shoes[index].image,
          price: shoes[index].price,
          featured: shoes[index].featured
        })
      }
    }
    res.send(shoesData)
  } catch (error) {
    res.status(400).send('Error')
    // console.log('Ada error')
  }
})

// Find All + Filter
router.get('/findAllFilter', async function (req, res) {
  try {
    var filter = {
      featured: req.body.featured
    }
    const shoes = await Shoes.find(filter)
    res.send(shoes)
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Delete One
router.delete('/deleteOne/:id', async function (req, res) {
  try {
    var myquery = { 
      _id: req.params.id 
    }

    var shoes = await Shoes.findOne(myquery)
    if (shoes) {
      // hapus image
      fs.unlink(imagepath + shoes.image, (err) => {
        if (err) throw err;
        console.log('image was deleted');
      });
    }

    await Shoes.deleteOne(myquery)
    res.send("1 document deleted")
  } catch (error) {
    // res.status(400).send('Error')
    console.log(error)
  }
})

// Delete Many
// router.delete('/deleteMany', async function (req, res) {
//   try {
//     var myquery = { 
//       address: "Highway 37 2"
//     }
//     var result = await Customers.deleteMany(myquery)
//     res.send(result.n + " document(s) deleted")
//   } catch (error) {
//     res.status(400).send('Error')
//   }
// })

// Update One
router.patch('/updateOne/:id', upload.single('image'), async function (req, res) {
  try {
    var myquery = { 
      _id: req.params.id
    }
    var image = req.file.filename

    var newvalues = { 
      $set: {
        name: req.body.name,
        address: req.body.address,
        image: image,
        brand: req.body.brand,
        price: req.body.price,
        desc: req.body.desc,
        featured: req.body.featured
        // colbg: req.body.colbg
      } 
    }

    var shoes = await Shoes.findOne(myquery)
    if (shoes) {
      // hapus image
      fs.unlink(imagepath + shoes.image, (err) => {
        if (err) throw err;
        console.log('path/file.txt was deleted');
      });
    }

    var result = await Shoes.updateOne(myquery, newvalues)
    res.send("1 document updated")
  } catch (error) {
    console.log('Error')
  }
})

// Update Many
// router.put('/updateMany', async function (req, res) {
//   try {
//     var myquery = { 
//       name: "Company Inc 2"
//     }
//     var newvalues = {
//       $set: {
//         name: "Minnie s"
//       } 
//     }
//     var result = await Customers.updateMany(myquery, newvalues)
//     res.send(result.nModified + " document(s) updated")
//   } catch (error) {
//     res.status(400).send('Error')
//   }
// })



module.exports = router
