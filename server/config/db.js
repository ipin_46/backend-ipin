const mongoose = require('mongoose')

// DB Option
const dbUrl = "mongodb+srv://ipin46:ipin46@cluster0-qfto2.mongodb.net/test"
const dbOption = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.Promise = global.Promise
mongoose.connect(dbUrl, dbOption)

module.exports = {
  mongoose: mongoose
}